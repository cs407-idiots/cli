# LucidLab CLI

Command-Line Interface for LucidLab, a heterogeneous, adaptable Wireless Sensor Networks testbed with support for IoT devices.

## Requirements
The CLI is provided in the form of a standalone Python script.
It requires Python 3.7 and the `requests` and `pyyaml` Python packages installed to run. Running `pip install requests pyyaml` should do the trick.

You can also run `pipenv install` and `pipenv shell` to enter the reproducible Python environment used in development, provided `pipenv` is installed.

## Usage
The script can be run by executing the Python file: `python lucidlab.py [ARGS]`

For convenience, a Linux shell script that does this is included: `./lucidlab [ARGS]` 
Making this command available system-wide requires that the directory be added to the `PATH` environment variable. You can do this temporarily by running `export PATH=$PWD:$PATH` from the CLI directory, or adding

```
export PATH=PATH_TO_CLI_FOLDER:$PATH
```

to your `~/.bashrc` script. Note that this has been tested only on Unix-based Operating Systems with POSIX-compliant shells (`bash`, `zsh`)

### Inbuilt documentation
The script includes help information.

`python lucidlab.py --help` lists the available commands and extra arguments

The system will elaborate on individual commands:
`python lucidlab.py list-configurations --help`

### Commands
The script is structured into sub-commands, in a similar fashion to other tools such as Git.
These commands and their options will be described below.

The format of a LucidLab CLI command is the following:

```
python lucidlab.py [GENERIC ARGUMENTS] command-name [COMMAND-SPECIFIC ARGUMENTS]
```

Generic arguments affect all commands. These arguments are discussed in more detail below, but in summary:
* `--raw` - dump raw data
* `--username/-u` - username for authentication
* `--password/-p` - password for authentication
* `--host` - change the hostname of the server. `localhost` by default.
* `--port` - change the port on which to access the server. `8080` by default.

Note that default values can be changed by modifying the CAPITALISED_VARIABLES at the top of `lucidlab.py`.

Command-specific arguments vary by the command used, and are discussed in their own sections.

### Raw output
Commands which retrieve data, such as `list-images`, can be instructed to dump data in raw format rather than as a user-friendly table. This is to aid automation efforts, which is the primary use case of this script!

Raw output can be turned on by adding the `--raw` flag to the script, **before the name of the command to run** - for example:

```
python lucidlab.py --raw --username USERNAME list-images
```

### Startup file
Simple configuration options can be specified in a YAML file. This file is `~/.lucidlab.yaml`, where ~ represents the user's home directory.

The keys recognised by this script are:
* `username` - username for authentication
* `password` - password for authentication
* `host` - change the hostname of the server. `localhost` by default.
* `port` - change the port on which to access the server. `8080` by default.

Note that these keys are **overridden** by explicit command-line arguments.

An example `~/.lucidlab.yaml`:

```yaml
username: USERNAME
password: PASSWORD
host: 12.34.56.78
port: 3000
```

### Authentication
Very important: many commands are user-specific and require authentication. Individual commands specify whether or not they require authentication in their `--help` documentation.

Authentication can be provided either via the command line, or via the `username` and `password` keys in the startup file, `~/.lucidlab.yaml`.

When using the command line, authentication is provided with the `--username` parameter. Passwords can be supplied inline with `--password`. These options must be specified **before the name of the command to run** - for example:

```
python lucidlab.py --username USERNAME --password PASSWORD list-images
```

**NOTE:** If this parameter is omitted the script will ask for authentication in an obscured text prompt, using Python's **getpass** library.

As mentioned previously, explicit command-line authentication **overrides the default settings specified in the startup file**.

The commands which require authentication are:
* `upload-*` commands
* `list-images`
* `list-configs`
* `list-iot-controllers`
* `list-jobs`, when listing a specific user's jobs - i.e. without the `--all` flag

### List of commands
#### Uploading an image: `upload-image`
Uploading an image requires some metadata. This data is supplied via positional arguments.
This command requires authentication.

In order:
1. Path to the image to upload. Should the path contain spaces, the whole path should be quoted.
2. Image board type. Options can be seen via the help command, `python lucidlab.py upload-image -h`.
3. Image OS type. Options can be seen via help command.

Additional metadata can be supplied via optional arguments. Omitting these arguments will send an empty string. If these strings require spaces, they must be quoted.
* A name for the image: `-n/--name`
* A description for the image: `-d/--description` 

For example:

```
python lucidlab.py --username USERNAME upload-image images/img.bin zolertia contiki --name "Test image" --description "Test image description"
```
#### Uploading a test configuration: `upload-config`
Uploading a configuration requires only the path to the config file, as all other metadata is contained in the file. Uploading a file validates it first; this validation can be performed without also scheduling a job via the `validate-config` command. This command requires authentication.

e.g. `python lucidlab.py --username USERNAME upload-config tests/config.yaml` 


#### Uploading an IoT controller: `upload-iot-controller`
Uploading an IoT controller requires a file path, and optionally supports `--name` and `--description` in the same fashion as an image. This command requires authentication.

e.g. `python lucidlab.py --username USERNAME upload-iot-controller controllers/controller.py --name "Test controller" --description "Test description"` 

#### Listing a user's uploaded test configurations: `list-configs`
Listing a user's uploaded configs is simple. Authenticate with `--username` and `--password`, and specify the `list-configs` command.

Note that the `--raw` command can be used to dump raw JSON data for automation.

#### Listing a user's uploaded images: `list-images`
Listing a user's uploaded images is simple. Authenticate with `--username` and `--password`, and specify the `list-images` command.

Note that the `--raw` command can be used to dump raw JSON data for automation.

#### Listing a user's uploaded IoT controllers: `list-iot-controllers`
Listing a user's uploaded IoT controllers is simple. Authenticate with `--username` and `--password`, and specify the `list-iot-controllers` command.

Note that the `--raw` command can be used to dump raw JSON data for automation.

#### Listing jobs: `list-jobs`
Listing the jobs on the system is a little more complex.

To show jobs from all users, provide the `--all` argument. To show jobs from a specific user, authenticate with `--username` and `--password` and omit the `--all` argument.

Three views of the jobs can be shown:
* All jobs of the specified user(s): no arguments
* Completed jobs only (jobs with results ready for download): `--completed`
* Scheduled jobs only (jobs that will be executed in the future, and cannot be downloaded yet): `--scheduled`

Please note that `--completed` and `--scheduled` cannot be used together.

e.g. `python lucidlab.py list-jobs --all --completed`

#### Listing IoT Things: `list-iot-things`
This command displays the IoT Things registered on the network. It takes no arguments (except `--raw`) and does not require authentication.

#### Listing registered motes: `list-motes`
This command displays the sensor motes registered on the network. It takes no arguments (except `--raw`) and does not require authentication.

#### Download commands
The CLI groups four download commands into one for brevity. To use these commands, specify `download` followed by:
* Test configuration YAML files: `config`
* Image binary files: `image`
* IoT controller Python files: `iot`
* Job results, as CSV files: `job`

The ID of the thing to download is then required - e.g. the image ID for an image.

These commands also allow an optional `-o/--output-file` argument, which specifies a file to download to. In the absence of this option, a default name based on the ID of the thing will be used.

example: `python lucidlab.py download job 3 --output-file job-3.csv`

For jobs, tests and IoT controllers, it's also possible to dump the file to the standard output stream instead of saving it. This can be done by providing the `--stdout` flag. This option doesn't work with images, as printing a binary file to the console tends to break things.

#### Delete commands
Delete commands are grouped in the same way as downloads, and use the same options.

e.g. `python lucidlab.py delete image 3` 

#### Validating a config file: `validate-config`
The `validate-config` command uploads a YAML file and checks its format without scheduling it for execution. Simply provide a path to the YAML file to upload. This command does not require authentication.

e.g.
```
python lucidlab.py validate-config "tests/config.yaml"
``` 

#### Creating a user: `create-user`
Creating a user is simple: supply the `--username`, `--password` and `--email` arguments to the `create-user` command. This command allows the username and password to be specified after the command for simplicity, since it also requires an email address.

```
python lucidlab.py create-user --username USERNAME --email USER@WEBSITE.COM --password PASSWORD
```

As before, omitting the password will ask for it in an obscured prompt powered by `getpass`. Note that for obvious reasons this command will **not** use username/password entries in the configuration file.

#### Getting the Received Signal Strength Indicator map data
The raw RSSI data used to build the map on the LucidLab metrics page can be retrieved via the `rssi` command. It takes no arguments and always returns raw JSON data, as pretty-printing the data as a matrix is difficult within the confines of a normal terminal.

Simply run the following:

```
python lucidlab.py rssi
```

#### Retrieving Clear Channel Assessment data
CCA data can be retrieved using the `cca` command. It supports two options. CCA data for a specific mote can be returned using `-m/--mote-id`, or global averages across all motes can be retrieved with `-g/--global-averages`.

Returned CCA values are a rolling average across 1000 tests, and are returned as an array of 16 values, one per channel.

For example:
```
python lucidlab.py cca --mote-id 15
```

#### Retrieving noise floor data
Noise floor data can be retrieved using the `noise-floor` command. It supports two options. Noise floor data for a specific mote can be returned using `-m/--mote-id`, or global averages across all motes can be retrieved with `-g/--global-averages`.

Returned noise floor values are a rolling average across 1000 tests, and are returned as an array of 16 values, one per channel.

For example:
```
python lucidlab.py noise-floor --mote-id 15
```
