import requests
import argparse
import os
import getpass
import sys
import re
import hashlib
import yaml

DEFAULT_HOST = 'localhost'
DEFAULT_PORT = 8080

IMAGE_TYPES = ['zolertia', 'telosb']
IMAGE_OSS = ['riot', 'contiki']

DEFAULT_USERNAME = None
DEFAULT_PASSWORD = None

CONFIG_FILE = '.lucidlab.yaml'


def load_config_file():
    global DEFAULT_HOST, DEFAULT_PORT, DEFAULT_USERNAME, DEFAULT_PASSWORD

    path = os.path.join(os.path.expanduser('~'), CONFIG_FILE)
    if not os.path.exists(path):
        return

    with open(path, 'r') as f:
        config = yaml.safe_load(f)

        if 'password' in config and 'username' in config:
            DEFAULT_USERNAME = config['username']
            DEFAULT_PASSWORD = config['password']

        if 'host' in config:
            DEFAULT_HOST = config['host']
        if 'port' in config:
            DEFAULT_PORT = int(config['port'])


def get_url(args, endpoint):
    host = args.host or DEFAULT_HOST
    port = args.port or DEFAULT_PORT

    return f'http://{host}:{port}/{endpoint}'


def authenticate(args):
    username = args.username or DEFAULT_USERNAME
    if not username:
        print("Please supply a username, either via"
              " -u/--username or by setting it in ~/.lucidlab.yaml")
        sys.exit(1)

    password = args.password or DEFAULT_PASSWORD or getpass.getpass(
        "LucidLab password: "
    )

    response = requests.post(
        get_url(args, 'test-login'),
        data={
            'username': username,
            'password': password
        }
    )
    if response.status_code == 200:
        user_id = response.json()['user_id']
        return user_id
    else:
        print("Error when authenticating:")
        print(response.text)
        sys.exit(1)


def handle_errors(response, message):
    if not response.status_code == 200:
        print(message)
        print(response.text)
        sys.exit(1)


def pretty_print_table(dict_list, col_list=None):
    """Pretty print a list of dictionaries as a table.
    Adapted from a solution by Thierry Husson:
    https://stackoverflow.com/a/40389411/1666687"""
    if not dict_list:
        raise TypeError
    if not col_list:
        col_list = list(dict_list[0].keys())
    # convert to Title Case
    header = list(map(lambda col: col.replace('_', ' ').title(), col_list))
    row_list = [header]

    for item in dict_list:
        row = [str(item[col] if item[col] else '')
               for col in col_list]
        row_list.append(row)

    # convert the table into a list of column widths
    col_sizes = [max(map(len, col)) for col in zip(*row_list)]
    format_string = ' | '.join(["{{:<{}}}".format(i) for i in col_sizes])

    # add separator row
    row_list.insert(1, ['-' * i for i in col_sizes])

    for item in row_list:
        print(format_string.format(*item))


parser = argparse.ArgumentParser(
    description="Command-Line Interface for LucidLab, an adaptable,"
    " heterogeneous testbed with support for IoT devices.\n"
    "To see help for a particular command, use the -h or --help flag: \n"
    "    lucidlab.py [COMMAND] --help",
    formatter_class=argparse.RawTextHelpFormatter
)
parser.add_argument(
    '-u',
    '--username',
    type=str,
    help='LucidLab username'
)
parser.add_argument(
    '-p',
    '--password',
    type=str,
    help='LucidLab password. Omit for a password prompt.'
)
parser.add_argument(
    '--host',
    type=str,
    help='Host address of LucidLab server'
)
parser.add_argument(
    '--port',
    type=str,
    help='Port on which LucidLab server is running'
)
parser.add_argument(
    '--raw',
    action='store_true'
)

subparsers = parser.add_subparsers(help='Commands')

# Upload commands


def upload_image(args):
    user_id = authenticate(args)
    if not args.raw:
        print(f"User ID: {user_id}")

    if not os.path.exists(args.image):
        print(f"File {args.image} does not exist.")
        sys.exit(1)

    with open(args.image, 'rb') as f:
        data = f.read()
        checksum = hashlib.sha1(data).hexdigest()

    with open(args.image, 'rb') as f:
        r = requests.post(
            get_url(args, 'upload-image'),
            data={
                'user_id': user_id,
                'image_type': args.image_type,
                'image_os': args.image_os,
                'image_name': args.name,
                'image_description': args.description,
                'checksum': checksum
            },
            files={'image': f}
        )

        handle_errors(r, 'Error uploading image:')
        id = r.json()['id']

        if not args.raw:
            print('Image upload successful.')
            print(f'Image ID: {id}')
        else:
            print(id)


upload_image_parser = subparsers.add_parser(
    'upload-image',
    help='Upload an image to LucidLab. '
    'Requires authentication with --username and --password'
)
upload_image_parser.add_argument(
    'image',
    type=str,
    help='Relative path to an image file.'
)
upload_image_parser.add_argument(
    'image_type',
    choices=IMAGE_TYPES,
    help='Mote type of the image to upload'
)
upload_image_parser.add_argument(
    'image_os',
    choices=IMAGE_OSS,
    help='Operating System of the image to upload'
)
upload_image_parser.add_argument(
    '-n',
    '--name',
    type=str,
    default='',
    help='Name of the image to upload, for easy identification'
)
upload_image_parser.add_argument(
    '-d',
    '--description',
    type=str,
    default='',
    help='A description for the image, for easy identification'
)

upload_image_parser.set_defaults(func=upload_image)


def upload_configuration(args):
    user_id = authenticate(args)
    if not args.raw:
        print(f"User ID: {user_id}")

    if not os.path.exists(args.config):
        print(f"File {args.config} does not exist.")
        sys.exit(1)

    with open(args.config, 'rb') as f:
        data = f.read()
        checksum = hashlib.sha1(data).hexdigest()

    with open(args.config, 'r') as f:
        r = requests.post(
            get_url(args, 'upload-configuration'),
            data={
                'user_id': user_id,
                'checksum': checksum
            },
            files={'yaml': f}
        )

        handle_errors(r, 'Error uploading configuration:')
        id = r.json()['id']
        time = r.json()['scheduled_time']
        job_id = r.json()['job_id']

        if not args.raw:
            print('Configuration upload successful.')
            print(f'Test config ID: {id}')
            print(f'Scheduled job ID: {job_id}')
            print(f'Scheduled time: {time}')
        else:
            print(id)
            print(job_id)
            print(time)


upload_config_parser = subparsers.add_parser(
    'upload-config',
    help='Upload a test configuration to LucidLab. '
    'Requires authentication with --username and --password'
)
upload_config_parser.add_argument(
    'config',
    type=str,
    help='Relative path to a configuration file.'
)
upload_config_parser.set_defaults(func=upload_configuration)


def upload_iot_controller(args):
    user_id = authenticate(args)
    if not args.raw:
        print(f"User ID: {user_id}")

    if not os.path.exists(args.iot_controller):
        print(f"File {args.iot_controller} does not exist.")
        sys.exit(1)

    with open(args.iot_controller, 'rb') as f:
        data = f.read()
        checksum = hashlib.sha1(data).hexdigest()

    with open(args.iot_controller, 'r') as f:
        r = requests.post(
            get_url(args, 'upload-iot-controller'),
            data={
                'user_id': user_id,
                'name': args.name,
                'description': args.description,
                'checksum': checksum
            },
            files={'controller': f}
        )

        handle_errors(r, 'Error uploading IoT controller:')
        id = r.json()['id']

        if not args.raw:
            print('IoT controller upload successful.')
            print(f'IoT controller ID: {id}')
        else:
            print(id)


upload_iot_controller_parser = subparsers.add_parser(
    'upload-iot-controller',
    help='Upload an IoT controller to LucidLab. '
    'Requires authentication with --username and --password'
)
upload_iot_controller_parser.add_argument(
    'iot_controller',
    type=str,
    help='Relative path to an IoT controller Python file.'
)
upload_iot_controller_parser.add_argument(
    '-n',
    '--name',
    type=str,
    help='Name of the IoT controller, for easy identification'
)
upload_iot_controller_parser.add_argument(
    '-d',
    '--description',
    type=str,
    help='Description for the IoT controller, for easy identification'
)
upload_iot_controller_parser.set_defaults(func=upload_iot_controller)


def list_images(args):
    user_id = authenticate(args)
    if not args.raw:
        print(f"User ID: {user_id}")

    r = requests.get(get_url(args, 'user-images'),
                     params={
                         'user_id': user_id
                     })
    handle_errors(r, 'Error getting images:')
    images = r.json()['images']
    if not images:
        print("No images.")
    else:
        if args.raw:
            print(str(images))
        else:
            pretty_print_table(images)


# Info retrieval endpoints
list_images_parser = subparsers.add_parser(
    'list-images',
    help='List a user\'s uploaded images. '
    'Requires authentication with --username and --password'
)
list_images_parser.set_defaults(func=list_images)


def list_configurations(args):
    user_id = authenticate(args)
    if not args.raw:
        print(f"User ID: {user_id}")

    r = requests.get(get_url(args, 'user-configurations'),
                     params={
                         'user_id': user_id
                     })
    handle_errors(r, 'Error getting configurations:')
    configs = r.json()['test_configs']
    if not configs:
        print("No test configurations.")
    else:
        if args.raw:
            print(str(configs))
        else:
            pretty_print_table(configs)


list_configs_parser = subparsers.add_parser(
    'list-configs',
    help='List a user\'s uploaded test configurations. '
    'Requires authentication with --username and --password'
)
list_configs_parser.set_defaults(func=list_configurations)


def list_iot_controllers(args):
    user_id = authenticate(args)
    if not args.raw:
        print(f"User ID: {user_id}")

    r = requests.get(get_url(args, 'user-iot-controllers'),
                     params={
                         'user_id': user_id
                     })
    handle_errors(r, 'Error getting IoT controllers:')
    controllers = r.json()['iot_controllers']
    if not controllers:
        print("No Iot Controllers.")
    else:
        if args.raw:
            print(str(controllers))
        else:
            pretty_print_table(controllers)


list_iot_controllers_parser = subparsers.add_parser(
    'list-iot-controllers',
    help='List a user\'s uploaded IoT controllers. '
    'Requires authentication with --username and --password'
)
list_iot_controllers_parser.set_defaults(func=list_iot_controllers)


def list_jobs(args):
    r = None
    if not args.all:
        user_id = authenticate(args)
        if not args.raw:
            print(f"User ID: {user_id}")

        endpoint = 'get-user-jobs'
        if args.scheduled:
            endpoint = 'get-user-scheduled-jobs'
        elif args.completed:
            endpoint = 'get-user-completed-jobs'

        r = requests.get(get_url(args, endpoint),
                         params={
                             'user_id': user_id
                         })
    else:
        completed = None
        if args.scheduled:
            completed = 'false'
        elif args.completed:
            completed = 'true'

        r = requests.get(get_url(args, 'get-all-jobs'),
                         params={
                             'completed': completed
                         })

    handle_errors(r, 'Error getting jobs:')
    jobs = r.json()['jobs']
    if not jobs:
        print("No jobs.")
    else:
        if args.raw:
            print(str(jobs))
        else:
            pretty_print_table(jobs)


list_jobs_parser = subparsers.add_parser(
    'list-jobs',
    help='List a user\'s uploaded IoT controllers. '
    'Requires authentication with --username and --password, '
    'unless the --all flag is specified.'
)
list_jobs_parser.add_argument(
    '--all',
    action='store_true',
    help='Flag to fetch jobs from all users. Auth not required if present.'
)
group = list_jobs_parser.add_mutually_exclusive_group()
group.add_argument(
    '-s',
    '--scheduled',
    action='store_true',
    help='Show only tests that have not yet been executed.'
)
group.add_argument(
    '-c',
    '--completed',
    action='store_true',
    help='Show only tests that have finished executing'
)
list_jobs_parser.set_defaults(func=list_jobs)


def list_motes(args):
    r = requests.get(get_url(args, 'mote-availability'))
    handle_errors(r, 'Error getting mote availability:')
    motes_dict = r.json()['status']

    if not motes_dict:
        if args.raw:
            print('[]')
        else:
            print("No motes registered.")
        sys.exit()
    motes = []
    for mote_id, item in motes_dict.items():
        motes.append({
            'mote_id': mote_id,
            'status': item['status'],
            'platform': item['platform']
        })
    if args.raw:
        print(str(motes))
    else:
        pretty_print_table(motes)


list_motes_parser = subparsers.add_parser(
    'list-motes',
    help='List sensor motes connected to the testbed.'
)
list_motes_parser.set_defaults(func=list_motes)


def list_iot_things(args):
    r = requests.get(get_url(args, 'get-iot-things'))
    handle_errors(r, 'Error getting IoT things:')
    things_dict = r.json()

    if not things_dict:
        if args.raw:
            print('[]')
        else:
            print("No IoT things registered.")
        sys.exit()

    things = []
    for name, label in things_dict.items():
        things.append({
            'name': name,
            'label': label
        })
    if args.raw:
        print(str(things))
    else:
        pretty_print_table(things)


list_iot_things_parser = subparsers.add_parser(
    'list-iot-things',
    help='List IoT things connected to the testbed.'
)
list_iot_things_parser.set_defaults(func=list_iot_things)


def download(args):
    endpoint = None
    params = None
    filename = None
    thing = args.thing_type

    if args.thing_type == 'image':
        endpoint = 'download-image'
        params = {
            'image_id': args.thing_id
        }
        if not args.raw:
            print(f'Downloading image with ID {args.thing_id}')
    elif args.thing_type == 'config':
        endpoint = 'get-test-config-file'
        params = {
            'test_id': args.thing_id
        }
        filename = f'{args.thing_id}.yaml'
        if not args.raw:
            print(f'Downloading test config with ID {args.thing_id}')
    elif args.thing_type == 'iot':
        endpoint = 'download-iot-controller'
        params = {
            'iot_controller_id': args.thing_id
        }
        thing = 'IoT controller'
        filename = f'{args.thing_id}.py'
        if not args.raw:
            print(f'Downloading IoT controller with ID {args.thing_id}')
    elif args.thing_type == 'job':
        endpoint = 'get-job-results-csv'
        params = {
            'job_id': args.thing_id
        }
        thing = 'job results'
        filename = f'{args.thing_id}.csv'
        if not args.raw:
            print(f'Downloading job results for job ID {args.thing_id}')
    else:
        raise Exception('Invalid thing to download: ' + args.thing_type)

    r = requests.get(get_url(args, endpoint), params=params)
    handle_errors(r, f'Error downloading {thing}:')

    if args.output_file is not None:
        filename = args.output_file
    elif args.thing_type == 'image':
        string = re.findall('filename=(.+)',
                            r.headers.get('content-disposition'))
        filename = string[0]

    if args.thing_type == 'image':
        # write binary data
        with open(filename, 'wb') as f:
            f.write(r.raw)
        if not args.raw:
            print(f'{thing.title()} saved to {filename}.')
    else:
        if args.stdout:
            print(r.text)
        else:
            with open(filename, 'w') as f:
                f.write(r.text)
            if not args.raw:
                print(f'{thing.title()} saved to {filename}.')


download_parser = subparsers.add_parser(
    'download',
    help='Download a CSV of a completed job\'s results'
)
download_parser.add_argument(
    'thing_type',
    choices=['config', 'image', 'iot', 'job'],
    help='The type of thing to download. Note that selecting '
    '\'job\' will download job results.'
)
download_parser.add_argument(
    'thing_id',
    type=int,
    help='ID of the thing to download.'
)
download_parser.add_argument(
    '-o',
    '--output-file',
    type=str,
    help='File to download to.')
download_parser.add_argument(
    '--stdout',
    action='store_true',
    help='Providing this flag will print the downloaded file to the '
    'CLI\'s standard output, instead of saving to a file. '
    'This option does not work for images.')
download_parser.set_defaults(func=download)


def delete(args):
    endpoint = None
    params = None
    thing = args.thing_type

    if args.thing_type == 'image':
        endpoint = 'delete-image'
        params = {
            'image_id': args.thing_id
        }
    elif args.thing_type == 'config':
        endpoint = 'delete-test'
        params = {
            'test_id': args.thing_id
        }
    elif args.thing_type == 'iot':
        endpoint = 'delete-iot-controller'
        params = {
            'iot_controller_id': args.thing_id
        }
        thing = 'IoT controller'
    elif args.thing_type == 'job':
        endpoint = 'delete-job'
        params = {
            'job_id': args.thing_id
        }
    else:
        raise Exception('Invalid thing to delete: ' + args.thing_type)

    r = requests.get(get_url(args, endpoint), params=params)
    handle_errors(r, f'Error deleting {thing}:')

    print(f'Successfully deleted {thing}.')


delete_parser = subparsers.add_parser(
    'delete',
    help='Delete an item.'
)
delete_parser.add_argument(
    'thing_type',
    choices=['image', 'config', 'iot', 'job'],
    help='The type of thing to be deleted'
)
delete_parser.add_argument(
    'thing_id',
    type=int,
    help='The ID of the image, config or IoT controller to delete'
)
delete_parser.set_defaults(func=delete)


def validate_configuration(args):
    with open(args.config, 'r') as f:
        r = requests.post(
            get_url(args, 'validate-yaml'),
            files={'yaml': f}
        )

        print(r.text)


validate_config_parser = subparsers.add_parser(
    'validate-config',
    help='Validate a test configuration to check its format is correct. '
)
validate_config_parser.add_argument(
    'config',
    type=str,
    help='Relative path to a configuration file.'
)
validate_config_parser.set_defaults(func=validate_configuration)


def create_user(args):
    if args.username is None:
        print('Missing required argument --username.')
        sys.exit(1)
    if args.email is None:
        print('Missing required argument --email.')
        sys.exit(1)

    password = args.password
    while password is None:
        password_1 = getpass.getpass('Enter a password: ')
        if getpass.getpass('Enter the password again: ') == password_1:
            password = password_1
            break
        print('Passwords do not match.')

    r = requests.post(get_url(args, 'create-user'),
                      data={
                          'username': args.username,
                          'email': args.email,
                          'password': password
                      })
    handle_errors(r, 'Error creating user:')
    d = r.json()
    if args.raw:
        print(d['id'])
    else:
        print(f'{d["message"]} User ID: {d["id"]}')


create_user_parser = subparsers.add_parser(
    'create-user',
    help='Create a new user with the given username/password combination.\n'
    'If the password option is omitted, the script will ask through an'
    ' obscured prompt.'
)
create_user_parser.add_argument(
    '-u',
    '--username',
    help='Username of account to create.'
)
create_user_parser.add_argument(
    '-p',
    '--password',
    help='Password for account to create.'
)
create_user_parser.add_argument(
    '-e',
    '--email',
    help='Email of account to create.'
)
create_user_parser.set_defaults(func=create_user)


def get_rssi(args):
    r = requests.get(get_url(args, 'get-rssi-map'))
    handle_errors(r, 'Error getting RSSI map:')

    # TODO print as table
    print(r.json()['map'])


rssi_parser = subparsers.add_parser(
    'rssi',
    help='Retrieve the current RSSI map for the testbed.'
)
rssi_parser.set_defaults(func=get_rssi)


def zip_ccas(cca_array):
    indices = [i for i in range(1, 17)]  # [1, 2, ...]
    zipped = zip(indices, cca_array)  # [(1, avg1)...]
    return [{
        'channel': channel_no,
        'average': average
    } for (channel_no, average) in zipped]


def get_cca(args):
    if args.global_averages:
        r = requests.get(get_url(args, 'get-cca-data'), params={
            'mote_id': -1
        })
        handle_errors(r, 'Error getting CCA values:')
        d = r.json()

        # zip channel indices up with cca values
        zipped = zip_ccas(d['global'])
        if not args.raw:
            print('Global rolling CCA averages across each channel '
                  'from 1000 tests:')
            if not zipped:
                print('No CCA averages returned. This is likely due to a '
                      'problem with the server.')
                sys.exit(1)
            pretty_print_table(zipped)
        else:
            print(zipped)
    else:
        r = requests.get(get_url(args, 'get-cca-data'), params={
            'mote_id': args.mote_id
        })
        handle_errors(r, 'Error getting CCA values:')
        d = r.json()

        # zip channel indices up with cca values
        zipped = zip_ccas(d['mote'])
        if not args.raw:
            print(f'Rolling CCA averages for mote {args.mote_id} '
                  'from 1000 tests:')
            if not zipped:
                print(f'Mote with ID {args.mote_id} not found.')
                sys.exit(1)
            pretty_print_table(zipped)
        else:
            print(zipped)


cca_parser = subparsers.add_parser(
    'cca',
    help='Retrieve Clear Channel Assessment info for the testbed.'
)
group = cca_parser.add_mutually_exclusive_group(required=True)
group.add_argument(
    '-m',
    '--mote-id',
    type=int,
    help='Mote ID of the sensor mote to retrieve CCA data from.'
)
group.add_argument(
    '-g',
    '--global-averages',
    action='store_true',
    help='Flag to retrieve global CCA averages.'
)
cca_parser.set_defaults(func=get_cca)


def get_noise_floor(args):
    if args.global_averages:
        r = requests.get(get_url(args, 'get-noise-floor-data'), params={
            'mote_id': -1
        })
        handle_errors(r, 'Error getting noise floor values:')
        d = r.json()

        # zip channel indices up with cca values
        zipped = zip_ccas(d['global'])
        if not args.raw:
            print('Global rolling noise floor averages across each channel '
                  'from 1000 tests:')
            if not zipped:
                print('No noise floor averages returned. '
                      'This is likely due to a problem with the server.')
                sys.exit(1)
            pretty_print_table(zipped)
        else:
            print(zipped)
    else:
        r = requests.get(get_url(args, 'get-noise-floor-data'), params={
            'mote_id': args.mote_id
        })
        handle_errors(r, 'Error getting noise floor values:')
        d = r.json()

        # zip channel indices up with data values
        zipped = zip_ccas(d['mote'])
        if not args.raw:
            print(f'Rolling noise floor averages for mote {args.mote_id} '
                  'from 1000 tests:')
            if not zipped:
                print(f'Mote with ID {args.mote_id} not found.')
                sys.exit(1)
            pretty_print_table(zipped)
        else:
            print(zipped)


noise_floor_parser = subparsers.add_parser(
    'noise-floor',
    help='Retrieve noise floor values for the testbed.'
)
group = noise_floor_parser.add_mutually_exclusive_group(required=True)
group.add_argument(
    '-m',
    '--mote-id',
    type=int,
    help='Mote ID of the sensor mote to retrieve noise floor data from.'
)
group.add_argument(
    '-g',
    '--global-averages',
    action='store_true',
    help='Flag to retrieve global noise floor averages.'
)
noise_floor_parser.set_defaults(func=get_noise_floor)


def run():
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    load_config_file()
    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    run()
